# European Linux Deployment Machines Setup #

## Machine assignments
Each team has been assigned an Amazon Linux 2 EC2 Instance for deployment.

The hostname of each machine is ```citiemealinuxN.conygre.com``` replace N with your group number.


## Starting/Stopping the linux machine
You can start and stop these linux machines in exactly the same way as the Windows VMs.

Go to <http://student.conygre.com>

Region is N.Virginia.


## Jenkins
Jenkins is running on your machine at port 8081. You can login to jenkins by going to (replace N with your number):
```http://citiemealinuxN.conygre.com:8080```

Jenkins can be started and stopped through systemctl e.g.:
```sudo systemctl stop jenkins```

Jenkins Login details:
```
username: admin
password: c0nygre
```


## Openshift
Openshift is running on your machine at port 8443. You can login to openshift by going to (replace N with your number):
```https://citiemealinuxN.conygre.com:8443```

Openshift can be started and stopped through systemctl e.g.:
```sudo systemctl stop okd```

Openshift Login details:
```
username: admin
password: admin
```


## SSH
You can login to the machine with SSH:
```
username: trainer
password: <ask an instructor> (the "usual" password)
```

e.g. ```ssh trainer@citiemealinuxN.conygre.com``` (replace N with your number)


## Docker
Docker is installed on the machines, some useful docker commands are:

* List all images saved to this machine: ```docker images```
* List all running containers on this machine: ```docker ps```
* List all running AND stopped containers on this machine: ```docker ps -a```
* Stop a running container: ```docker stop <container-name>```
* View the logs of a running container ```docker logs <container-name>``` e.g. ```docker logs dummy-trade-filler```
