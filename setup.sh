## This is citieurlinuxN.conygre.com ##

sudo usermod -aG docker trainer
sudo usermod -aG docker grads
sudo service openshift stop

docker network create mongo-net
docker run --name mongo --restart unless-stopped  -d -p 27017:27017 --net mongo-net -v ~/data:/data/db mongo
sudo yum -y install git

sudo amazon-linux-extras install -y java-openjdk11
sudo yum install -y java-11-openjdk-devel
echo 'export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64' >> /home/grads/.bashrc

sudo /usr/sbin/alternatives --set java /usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64/bin/java
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64
git clone https://fcallaly@bitbucket.org/fcallaly/dummy-trade-filler.git
sudo chown -R grads: dummy-trade-filler
cd dummy-trade-filler
./gradlew build
sudo chown -R grads: /home/grads/dummy-trade-filler

docker build -t dummy-trade-filler:0.0.1 .
docker run --name dummy-trade-filler --restart unless-stopped -d --net mongo-net -e DB_HOST=mongo dummy-trade-filler:0.0.1


## MongoDB will run in a container. To get a mongo command line
# run the below command (without the leading #)
# docker exec -it mongo mongo

